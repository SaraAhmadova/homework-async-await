const findIpBtn = document.querySelector(".find-btn");
const container = document.querySelector(".container");

const findIp = async () => {
  try {
    const response = await fetch('https://api.ipify.org/?format=json');
    const data = await response.json();
    const ip = data.ip;
    return ip
  } catch (error) {
    console.error(error);
  }
};

const getCountryData = async () => {
  try {
    const ip = await findIp();
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=66842623&lang=en`);
    const data = await response.json();
   return data
  } catch (error) {
    console.error(error);
  }
};

const displayAddress = async () => {
  try {
      const countryData = await getCountryData();
      document.querySelector('#continent').textContent = `Continent: ${countryData.continent}`;
      document.querySelector('#country').textContent = `Country: ${countryData.country}`;
      document.querySelector('#region').textContent = `Region: ${countryData.region}`;
      document.querySelector('#city').textContent = `City: ${countryData.city}`;
      document.querySelector('#district').textContent = `District: ${countryData.district}`;
  } catch (error) {
      container.innerHTML = 'Error occurred while fetching data.';
      console.error(error);
  }
};


findIpBtn.addEventListener("click", displayAddress);
